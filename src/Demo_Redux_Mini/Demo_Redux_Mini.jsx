import React, { Component } from "react";
import { connect } from "react-redux";
import {
  TANG_SO_LUONG,
  GIAM_SO_LUONG,
} from "./redux/constants/numberConstants.js";
import {
  tangSoLuongAction,
  giamSoLuongAction,
} from "./redux/action/numberAction.js";
class Demo_Redux_Mini extends Component {
  render() {
    return (
      <div>
        <button onClick={this.props.tangSoLuong} className="btn btn-warning">
          +
        </button>
        <span>{this.props.soLuong}</span>
        <button onClick={this.props.giamSoLuong} className="btn btn-success">
          -
        </button>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    soLuong: state.numberReducer.number,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    tangSoLuong: () => {
      dispatch(tangSoLuongAction());
    },
    giamSoLuong: () => {
      dispatch(giamSoLuongAction());
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Demo_Redux_Mini);
