import logo from "./logo.svg";
import "./App.css";
import Demo_Redux_Mini from "./Demo_Redux_Mini/Demo_Redux_Mini.jsx";
function App() {
  return (
    <div className="App">
      <Demo_Redux_Mini />
    </div>
  );
}

export default App;
