import { TANG_SO_LUONG, GIAM_SO_LUONG } from "../constants/numberConstants";

export const tangSoLuongAction = () => {
  return {
    type: TANG_SO_LUONG,
    payload: 1,
  };
};
export const giamSoLuongAction = () => {
  return {
    type: GIAM_SO_LUONG,
    payload: -1,
  };
};
