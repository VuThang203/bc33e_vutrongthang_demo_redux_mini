import { combineReducers } from "redux";
import { numberReducer } from "../redux/numberReducers";

export let rootReducer_demoReduxMini = combineReducers({
  numberReducer,
});
