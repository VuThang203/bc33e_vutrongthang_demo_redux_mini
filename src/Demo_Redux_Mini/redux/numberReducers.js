import {
  TANG_SO_LUONG,
  GIAM_SO_LUONG,
} from "../redux/constants/numberConstants";
let initialState = {
  number: 10,
};

export const numberReducer = (state = initialState, action) => {
  switch (action.type) {
    case TANG_SO_LUONG: {
      state.number += action.payload;
      return { ...state };
    }
    case GIAM_SO_LUONG: {
      state.number += action.payload;
      return { ...state };
    }
    default:
      return state;
  }
};
